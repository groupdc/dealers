/*
Navicat MySQL Data Transfer

Source Server         : Combell_iis
Source Server Version : 50623
Source Host           : 178.208.48.50:3306
Source Database       : dcm_dcms

Target Server Type    : MYSQL
Target Server Version : 50623
File Encoding         : 65001

Date: 2018-12-07 10:22:23
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `dealers`
-- ----------------------------
DROP TABLE IF EXISTS `dealers`;
CREATE TABLE `dealers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(36) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dealer` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'dealer name',
  `attention` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 't.av. extra DPD info',
  `address` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zip` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `regiocode` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `city` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country_id` int(11) unsigned DEFAULT '1',
  `phone` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contactemail` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'email voor naturapy bestellingen',
  `website` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `code` varchar(50) COLLATE utf8_unicode_ci DEFAULT '',
  `code_NL` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'exactcode',
  `longitude` decimal(13,7) DEFAULT NULL,
  `latitude` decimal(13,7) DEFAULT NULL,
  `admin` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `i_CODE` (`code`),
  KEY `FK_dealercountry` (`country_id`),
  CONSTRAINT `FK_dealercountry` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7197 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='rating, premium, partner omzetten naar 1 veld: TYPE';

-- ----------------------------
-- Table structure for `dealers_closure`
-- ----------------------------
DROP TABLE IF EXISTS `dealers_closure`;
CREATE TABLE `dealers_closure` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `dealer_id` int(10) unsigned DEFAULT NULL,
  `closure_startdate` datetime DEFAULT NULL,
  `closure_enddate` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `admin` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_dealer_id` (`dealer_id`),
  CONSTRAINT `FK_dealer_id` FOREIGN KEY (`dealer_id`) REFERENCES `dealers` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=181 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for `dealers_opening`
-- ----------------------------
DROP TABLE IF EXISTS `dealers_opening`;
CREATE TABLE `dealers_opening` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `dealer_id` int(10) unsigned DEFAULT NULL,
  `opening_day` smallint(1) DEFAULT NULL,
  `opening_hour_1` time DEFAULT NULL,
  `opening_hour_2` time DEFAULT NULL,
  `opening_hour_3` time DEFAULT NULL,
  `opening_hour_4` time DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `admin` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_dealer_id` (`dealer_id`),
  CONSTRAINT `dealers_opening_ibfk_1` FOREIGN KEY (`dealer_id`) REFERENCES `dealers` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6340 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for `dealers_to_pages`
-- ----------------------------
DROP TABLE IF EXISTS `dealers_to_pages`;
CREATE TABLE `dealers_to_pages` (
  `dealer_id` int(10) unsigned NOT NULL,
  `page_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `FK_dealerpageid` (`page_id`),
  KEY `FK_dealerpagedealerid` (`dealer_id`),
  CONSTRAINT `FK_dealerpagedealerid` FOREIGN KEY (`dealer_id`) REFERENCES `dealers` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `FK_dealerpageid` FOREIGN KEY (`page_id`) REFERENCES `pages_language` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DELIMITER ;;
CREATE TRIGGER `before_insert_dealer` BEFORE INSERT ON `dealers` FOR EACH ROW BEGIN
    SET new.uuid = uuid();
END
;;
DELIMITER ;
