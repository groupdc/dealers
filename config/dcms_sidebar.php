<?php
return [
    "Dealers" => [
        "icon"  => "fa-map-marker",
        "links" => [
            ["route" => "/admin/dealers", "label" => "Overview", "permission" => 'dealers'],
            ["route" => "/admin/dealers/map", "label" => "Map", "permission" => 'dealers']
        ],
    ],
];

?>
