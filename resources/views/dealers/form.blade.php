@extends("dcms::template/layout")

@section("content")

    <div class="main-header">
        <h1>Dealers</h1>
        <ol class="breadcrumb">
            <li><a href="{!! URL::to('admin/dashboard') !!}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="{!! URL::to('admin/dealers') !!}"><i class="fa fa-pencil"></i> Dealers</a></li>
            @if(isset($dealer))
                <li class="active">Edit</li>
            @else
                <li class="active">Create</li>
            @endif
        </ol>
        @if(isset($dealer))
            <p class="edit">Last edited by {{ $dealer->admin }} on {{ Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $dealer->updated_at)->format('d-m-Y H:i:s') }}</p>
        @endif
    </div>

    <div class="main-content">
        <div class="row">

            @if(isset($dealer))
                {!! Form::model($dealer, array('route' => array('admin.dealers.update', $dealer->id), 'method' => 'PUT')) !!}
            @else
                {!! Form::open(array('url' => 'admin/dealers')) !!}
            @endif



            @if(!is_null($extendformTemplate["template"]))
                @include($extendformTemplate["template"], array('dealer'=>(isset($dealer)?$dealer:null)))
            @endif

            <div class="col-md-12">
                <div class="main-content-tab tab-container">

                    <ul class="nav nav-tabs" role="tablist">
                        <li class="active"><a href="#data" role="tab" data-toggle="tab">Data</a></li>
                        <li><a href="#closures" role="tab" data-toggle="tab">Closure</a></li>
                        <li><a href="#openings" role="tab" data-toggle="tab">Openings</a></li>
                    </ul>

                    <div class="tab-content">

                        @if($errors->any())
                            <div class="alert alert-danger">{!! HTML::ul($errors->all()) !!}</div>
                        @endif

                        <div id="data" class="tab-pane active">

                            <div class="form-group">
                                {!! Form::label('dealer', 'Dealer') !!}
                                {!! Form::text('dealer', null, array('class' => 'form-control')) !!}
                            </div>

                            <div class="form-group">
                                {!! Form::label('address', 'Address') !!}
                                {!! Form::text('address', null, array('class' => 'form-control')) !!}
                            </div>

                            <div class="row">
                                <div class="col-sm-2">

                                    <div class="form-group">
                                        {!! Form::label('zip', 'Zip') !!}
                                        {!! Form::text('zip', null, array('class' => 'form-control')) !!}
                                    </div>

                                </div>
                                <div class="col-sm-10">
                                    <div class="form-group">
                                        {!! Form::label('city', 'City') !!}
                                        {!! Form::text('city', null, array('class' => 'form-control')) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                {!! Form::label('country_id', 'Country') !!}
                                {!! Form::select('country_id', $countries, old('country_id'), array('class' => 'form-control')) !!}
                            </div>

                            <div class="form-group">
                                {!! Form::label('longitude', 'longitude') !!}
                                {!! Form::text('longitude', null, array('class' => 'form-control')) !!}
                            </div>

                            <div class="form-group">
                                {!! Form::label('latitude', 'Latitude') !!}
                                {!! Form::text('latitude', null, array('class' => 'form-control')) !!}
                            </div>


                            <div class="form-group">
                                {!! Form::label('phone', 'Phone') !!}
                                {!! Form::text('phone', null, array('class' => 'form-control')) !!}
                            </div>

                            <div class="form-group">
                                {!! Form::label('email', 'Email') !!}<span class="hidden">Shown on the dealer page on the websites</span>
                                {!! Form::text('email', null, array('class' => 'form-control')) !!}
                            </div>

                            <div class="form-group">
                                {!! Form::label('contactemail', 'Contact email') !!} <span class="hidden">Naturapy&reg;</span>
                                {!! Form::text('contactemail', null, array('class' => 'form-control')) !!}
                            </div>

                            <div class="form-group">
                                {!! Form::label('website', 'Website') !!}
                                {!! Form::text('website', "http://".str_replace(["http://", "https://"],"",(isset($dealer->website)?$dealer->website:old('website'))), array('class' => 'form-control')) !!}
                            </div>

                        </div>


                            <div id="closures" class="tab-pane">

                                <div id="closure">
                                    <table class="table table-bordered table-striped ">
                                        <thead>
                                        <tr>
                                            <th>Startdate dd/mm/yyyy (first day of closing)</th>
                                            <th>Enddate dd/mm/yyyy (last day of closing)</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        @if(isset($rowClosure))
                                            {!! $rowClosure !!}
                                        @endif
                                        <tfoot>
                                        <tr>
                                            <td colspan="5"><a class="btn btn-default pull-right add-table-row" href=""><i
                                                            class="fa fa-plus"></i></a></td>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>

                            </div>

                            <div id="openings" class="tab-pane">

                                @for($i = 1; $i <= 7; $i++)
                                    <div class="row">
                                        <div class="col-xs-6 col-md-2">
                                            <div class="form-group">
                                                <p class="form-control-static" style="margin: 10px 0"><b>{{ jddayofweek($i - 1, 1) }}</b></p>
                                            </div>
                                        </div>

                                        <div class="col-xs-6 col-md-2">
                                            {!! Form::checkbox('open_'.$i , 1, $openings->where('opening_day', $i)->first(), array('class' => 'form-checkbox' , 'id' => 'open_'.$i)) !!}
                                            {!! Form::label('open_'.$i, "&nbsp;", array('class' => ($openings->where('opening_day', $i)->first())?'checkbox active':'checkbox')) !!}
                                        </div>
                                        @for($j = 1; $j <= 4; $j++)
                                            <div class="col-xs-6 col-md-2">
                                                <div class="form-group">
                                                    <input type="time" id="opening_hour_{{$j}}[{{$i}}]" name="opening_hour_{{$j}}[{{$i}}]"
                                                           class="form-control"
                                                           value="{{ $errors->any() ? old('opening_hour_' . $j . '.' . $i) : ($openings->where('opening_day', $i)->first() && $openings->where('opening_day', $i)->first()->{'opening_hour_' . $j} ? Carbon\Carbon::createFromFormat('H:i:s', $openings->where('opening_day', $i)->first()->{'opening_hour_' . $j})->format('H:i') : null)}}">
                                                </div>
                                            </div>
                                        @endfor
                                    </div>
                                @endfor

                                <div class="row">

                                    <div class="col-xs-6 col-md-2">
                                        <div class="form-group">
                                            AM (00:00 - 11:59)<br/>
                                            PM (12:00 - 23:59)<br/><br/>
                                        </div>
                                    </div>

                                    <div class="col-xs-6 col-md-2">
                                        <div class="form-group">
                                            12:00 AM = midnight (00:00)<br/>
                                            12:00 PM = midday (12:00)
                                        </div>
                                    </div>

                                </div>
                            </div>

                    </div><!-- end tab-content -->
                </div><!-- end main-content-tab -->
            </div><!-- end col-md-12 -->

            <div class="col-md-12">
                <div class="main-content-block">
                    {!! Form::submit('Save', array('class' => 'btn btn-primary')) !!}
                    <a href="{!! URL::previous() !!}" class="btn btn-default">Cancel</a>
                </div>
            </div>

            {!! Form::close() !!}

        </div><!-- end row -->
    </div><!-- end main-content -->

@stop

@section("script")

    <script type="text/javascript">
        $(document).ready(function () {

            //Bootstrap Tabs
            $(".tab-container .nav-tabs a").click(function (e) {
                e.preventDefault()
                $(this).tab('show')
            })

            //UI Autocomplete Product Detail
            $(".main-content-block input[id='zip'], .main-content-block input[id='city']").autocomplete({
                source: function (request, response) {
                    var country_id = $("#country_id").val();
                    $.getJSON("{!! route('admin.dealers.api.zipcity') !!}?zipcity=" + request.term + "&country_id=" + country_id, function (data) {
                        response(data);
                    });
                },
                select: function (event, ui) {
                    $(this).val(ui.item.label);
                    $(this).closest(".main-content-block").find("input[id='zip']").val(ui.item.zip);
                    $(this).closest(".main-content-block").find("input[id='city']").val(ui.item.city);
                    return false;
                },
                minLength: 1,
                delay: 200
            });

        });
    </script>

    <script type="text/javascript" src="{!! asset('packages/dcms/core/js/bootstrap.min.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('packages/dcms/core/js/jquery-ui-autocomplete.min.js') !!}"></script>
    <link rel="stylesheet" type="text/css" href="{!! asset('packages/dcms/core/css/jquery-ui-autocomplete.css') !!}">

    <script language="javascript" type="application/javascript">

        $(document).ready(function () {



            //Add table row
            $.fn.addtablerow = function (options) {

                $(this).each(function () {

                    var table = $(this);

                    var rows = table.closest('tbody tr').length;

                    table.find('.add-table-row').click(function () {
                        //  language_id = table.attr("class").replace('table table-bordered table-striped ','');
                        geturl = options.source;
                        //  geturl = geturl.replace("{LANGUAGE_ID}",language_id);

                        $.get(geturl, function (data) {
                            if (!table.find('tbody').length) table.find('thead').after("<tbody></tbody>");

                            data = data.replace(/{INDEX}/g, "extra" + "-" + rows);
                            table.find('tbody').append(data);
                            //$("#attachment-language-id[extra"+rows+"] option[value='"+language_id+"']").attr('selected','selected');
                            rows++;
                            deltablerow(table.find('.delete-table-row').last());
                        });
                        return false;
                    });

                    deltablerow(table.find('.delete-table-row'));

                    function deltablerow(e) {
                        e.click(function () {
                            $(this).closest("tr").remove();
                            if (!table.find('tbody tr').length) table.find('tbody').remove();
                            return false;
                        });
                    }

                });

            };


            $("#closure table").addtablerow({
                source: "{!! URL::to('admin/dealers/api/closurerow?data=closure') !!}" //generate the row with the dropdown fields/empty boxes/etc.
            });
        });

    </script>

@stop
